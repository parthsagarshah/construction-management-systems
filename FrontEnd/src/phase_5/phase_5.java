package phase_5;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SreedharReddy
 */
public class phase_5 {
    
    
 public static Connection conn=null;

    
    Statement stmt=null;
    //constructor
    public phase_5(){} 

    //Function for Database connectivity.
    public  void DBConnection()
    {
        String url = "jdbc:oracle:thin:@localhost:1521:CSE1";
        String db_id="sxa0453";
        String db_pwd="Ab112233";
        try
        {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn=DriverManager.getConnection(url, db_id, db_pwd);
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("LOADING ERROR:"+e.toString());
        }
        catch(SQLException se)
        {
            System.out.println("FAIL OF DB CONNECTION  : "+se.toString());               
        }
        catch(Exception ee)
        {
            System.out.println("Exception in connection : "+ee.toString());
            
        }


    }
     int does_pid_exists(String PID) {
        int flag=0;
        try 
        {
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_PERSONNEL");
            
            // Fetch each row from the result set
            while (rs.next()) 
            {
                // Get the data from the row using the column index
                
                if(rs.getString(1).equals(PID))
                {
                    
                       flag=1;
                       break;
                    
                }
    
            }
            stmt.close();
        } 
        
        catch (SQLException e) 
        {
        }     
        return flag;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        int does_I_id_exists(String Inv_ID) {
        int flag=0;
        try 
        {
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_INVENTORY");
            
            // Fetch each row from the result set
            while (rs.next()) 
            {
                // Get the data from the row using the column index
                
                if(rs.getString(1).equals(Inv_ID))
                {
                    
                       flag=1;
                       break;
                    
                }
    
            }
            stmt.close();
        } 
        
        catch (SQLException e) 
        {
        }     
        return flag;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        int does_M_id_exists(String M_ID) {
        int flag=0;
        try 
        {
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_MACHINERY");
            
            // Fetch each row from the result set
            while (rs.next()) 
            {
                // Get the data from the row using the column index
                
                if(rs.getString(1).equals(M_ID))
                {
                    
                       flag=1;
                       break;
                    
                }
    
            }
            stmt.close();
        } 
        
        catch (SQLException e) 
        {
        }     
        return flag;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    void display_Project()
    {
        try 
        {
            int count;
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_PROJECT");
            count=8;
            
            // Fetch each row from the result set
            System.out.println("Project ID\tProject Name\t Estd_Budg\t Location\t Land_cost\t Dimentsion\t  St_Date\t End_date ");
            while (rs.next()) {
                int i=0;
//
// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }
    
    
    void display_Personnel()
    {
        try 
        {
            int count;
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_PERSONNEL");
            count=2;
            
            // Fetch each row from the result set
            System.out.println("Personnel_ID\tpersonnel_name :");
            while (rs.next()) {
                int i=0;
//
// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }
    void display_Supplier()
    {
        try 
        {
            int count;
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_SUPPLIER");
            count=2;
            
            // Fetch each row from the result set
            System.out.println("\nSupplier_ID, S_name :");
            while (rs.next()) {
                int i=0;
//
// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }
    void display_Inventory()
    {
        try 
        {
            int count;
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM F15_7_INVENTORY");
            count=4;
            
            // Fetch each row from the result set
            System.out.println("\nInv_ID \t Inv_name \t Inv_Type \t Inv_COst");
            while (rs.next()) {
                int i=0;
//
// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }
    
    void new_Personnel(String Person_ID) throws IOException
    {
         InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String P_name="";
       
        System.out.println("\nEnter Personnel name: "); 
        P_name=stdin.readLine();
        try
        {
             String sqlst="INSERT INTO F15_7_PERSONNEL VALUES (?,?)";
            PreparedStatement stmt1= null;
                  stmt1=  conn.prepareStatement(sqlst);
                   stmt1.setString(1,Person_ID);
                  stmt1.setString(2,P_name);
                  stmt1.executeQuery();
        //DBConnection();
       // String sqlst="INSERT INTO F15_7_PERSONNEL VALUES (?,?)";
        
       // stmt.executeQuery(sqlst);
        System.out.println("insertion into personnel is done");
        stmt1.close();
    }
        catch(SQLException e)
        {
        
            System.out.println(e.toString());
        }
    }
    void new_Inventory() throws IOException
    {
         InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String inv_Id="", I_name="",type="",cost="";
        System.out.println("\nEnter Inventory ID: ");    
        inv_Id=stdin.readLine();
        System.out.println("\nEnter Inventory name: "); 
        I_name=stdin.readLine();
        System.out.println("\nEnter Inventory type: "); 
        type=stdin.readLine();
        System.out.println("\nEnter Cost: "); 
        cost=stdin.readLine();
        try
        {
        
       //DBConnection();
            String sqlst="INSERT INTO F15_7_inventory VALUES (?,?,?,?)";
            PreparedStatement stmt1= null;
                  stmt1=  conn.prepareStatement(sqlst);
                   stmt1.setString(1,inv_Id);
                  stmt1.setString(2,I_name);
                  stmt1.setString(3,type);
                  stmt1.setString(4,cost);
                  stmt1.executeUpdate();
        System.out.println("insertion successfull");
        
        stmt1.close();
    }
        catch(SQLException e)
        {
        
            System.out.println(e.toString());
        }
    }

    void new_Project() throws IOException, ParseException
    {
        
         InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String Proj_id="",Estd_Budget="",Land_cost="";
        String Dimensions="";
        String PName="", Location="";
        Date St_Date,End_Date;
        System.out.println("\nEnter Project ID: ");    
        Proj_id=stdin.readLine();
        System.out.println("\nEnter Project Name: "); 
        PName=stdin.readLine();
        System.out.println("\nEnter Estimated Budget: "); 
        Estd_Budget=stdin.readLine();
        System.out.println("\nEnter Location: "); 
        Location=stdin.readLine();
        System.out.println("\nLand Cost: "); 
        Land_cost=stdin.readLine();
        System.out.println("\nEnter Dimensions: "); 
        Dimensions=stdin.readLine();
        System.out.println("\nEnter Start Date: "); 
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy");     
        St_Date=sdf.parse(stdin.readLine());
        System.out.println("\nEnter End Date: "); 
        End_Date=sdf.parse(stdin.readLine());
        
        try
        {
        //stmt=conn.createStatement();
        //DBConnection();
        String sqlst="INSERT INTO F15_7_PROJECT VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement stmt1= null;
                  stmt1=  conn.prepareStatement(sqlst);
                   stmt1.setString(1,Proj_id);
                  stmt1.setString(2,PName);
                  stmt1.setString(3,Estd_Budget);
                  stmt1.setString(4,Location);
                  stmt1.setString(5,Land_cost);
                  stmt1.setString(6,Dimensions);
                  stmt1.setDate(7,new java.sql.Date(St_Date.getTime()));
                  stmt1.setDate(8,new java.sql.Date(End_Date.getTime()));
                 // stmt1.executeUpdate();
        
                  stmt1.executeUpdate();
                  System.out.println("insertion successfull");
        stmt1.close();
        }
         catch(SQLException e)
        {
        
            System.out.println(e.toString());
        }
    }

    void update_Inventory_Cost() throws IOException
    {
        InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String Inv_ID="", Inv_cost="";
        System.out.println("\nEnter Inventory ID to change cost: ");    
        Inv_ID=stdin.readLine();
        int x=0;
        x=this.does_I_id_exists(Inv_ID);
        if(x==0)
            System.out.println("Inventory id doesnt exist");
        else
        {
        System.out.println("\nEnter new inventory cost: "); 
        Inv_cost=stdin.readLine();
        
        try 
        {
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            stmt.executeQuery("UPDATE F15_7_INVENTORY SET COST='"+Inv_cost+"' WHERE INV_ID='"+Inv_ID+"'");
            System.out.println("\nUpdation Done!");
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
        }
    }
    void update_Machinery_Cost() throws IOException
    {
        InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String M_ID="", Cost_perday="";
        System.out.println("\nEnter Machinery ID to change cost: ");    
        M_ID=stdin.readLine();
        int x=0;
        x=this.does_M_id_exists(M_ID);
        if(x==0)
            System.out.println("Machinery id doesnt exist");
        else
        {
        System.out.println("\nEnter new Machinery cost: "); 
        Cost_perday=stdin.readLine();
        
        try 
        {
            // Create a result set containing all data from my_table
            stmt = conn.createStatement();
            stmt.executeQuery("UPDATE F15_7_MACHINERY SET COST_PERDAY='"+Cost_perday+"' WHERE M_ID='"+M_ID+"'");
            System.out.println("\nUpdation Done!");
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
        }
    }

    void delete_Personnel() throws IOException
    {
    
         InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        String Person_ID="";
        System.out.println("\nEnter Personnel ID: ");    
        Person_ID=stdin.readLine();
        int x=0;
        x=this.does_pid_exists(Person_ID);
        if(x==0)
            System.out.println("Personnel id doesnt exist");
        else
        {
        try
        {
        stmt=conn.createStatement();
        //DBConnection();
        String sqlst="DELETE FROM F15_7_PERSONNEL WHERE P_ID='"+Person_ID+"'";
        //System.out.println("deletion done!!!");
        
        stmt.executeQuery(sqlst);
        System.out.println("deletion done!!!");
        stmt.close();
    }
        catch(SQLException e)
        {
        
            System.out.println(e.toString());
        }
        }
    }
      public void query1() throws IOException
    {

        try 
        {
            
            InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
            String Proj_id="",cost="";
            System.out.println("Enter Project Id");
            Proj_id=stdin.readLine();
            int x=0;
        x=this.does_pid_exists(Proj_id);
        if(x==0){
            System.out.println("Project id doesnt exist");
        }
        else
        {
            System.out.println("Enter the cost to be cross checked by");
            cost=stdin.readLine();
            
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select m.M_ID,r.prj_id,m.cost_perday from f15_7_machinery m, f15_7_rent r where m.m_id=r.m_id and r.prj_id="+Proj_id+" and m.cost_perday >"+cost+" order by m.cost_perday");
            
            // Fetch each row from the result set
             int count= 3;
            // Fetch each row from the result set
            System.out.println("M_ID\tP_ID\tCost per Day");
           while (rs.next()) {
                int i1=0;
//5

// Display the Results
//
                 while(i1<count){
	            i1=i1+1;
                    System.out.print(rs.getString(i1)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        }
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        } 
        
    }
    public void query2()
    {

        try 
        {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select l_id,type from F15_7_labourer l, F15_7_works_On_Duration w where l.l_id=w.per_id and w.per_id in(select per_id from F15_7_works_On_Duration w,F15_7_labourer l where w.per_id=l.l_id and w.hoursperday = (select max(hoursperday) from F15_7_works_On_Duration))");
            int count= 2;
            System.out.println("Labour id\ttype");
            // Fetch each row from the result set
           while (rs.next()) {
                int i=0;

// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }     
    }
    
    public void query3() throws IOException
    {
        try 
        {
            
            InputStreamReader i1 = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i1);
            String No_of_Project="",Salary="";
            int sal=0;
            int num=0;
            System.out.println("Enter Number of projects he should be working on");
            No_of_Project=stdin.readLine();
            System.out.println("Enter Salary to compare with");
            Salary=stdin.readLine();
            sal=Integer.parseInt(Salary);
            num=Integer.parseInt(No_of_Project);
            
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select w.per_id, (select E.position from F15_7_employee E where E.E_ID = w.per_id) AS \"Employee position\", COUNT (*) AS \"No of Projects\" from F15_7_works_On_Duration w, F15_7_employee e where e.e_id=w.per_id and e.montly_sal >"+sal+" group by (w.per_id) having count (*) >="+num+"");
            
                  System.out.println("Personnel ID\type");
            // Fetch each row from the result set
             int count= 2;
        
            // Fetch each row from the result set
           while (rs.next()) {
                int i=0;

// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            
            stmt.close();
            
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }
  /*  public void query4() throws IOException
    {
        try 
        {
            
            InputStreamReader i1 = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i1);
            String Proj_Id="",Time="";
            System.out.println("Enter Project ID");
            Proj_Id=stdin.readLine();
             int x=0;
        x=this.does_pid_exists(Proj_Id);
        if(x==1)
            System.out.println("Project id doesnt exist");
        else
        {
            System.out.println("Enter Number of Months");
            Time=stdin.readLine();
            
            //stmt = conn.createStatement();
            String query_4 = "select sum(i.cost*s.quantity) as sum, proj_id from F15_7_inventory i, F15_7_supply s,F15_7_project p where i.inv_id=s.in_id and p.proj_id=s.prj_id and s.s_date between p.start_date and add_months(p.start_date,"+Time+") group by p.proj_id having p.proj_id ="+Proj_Id+"";
            stmt=conn.createStatement();
            //stm1.setString(1,Time);
            //stm1.setString(2,Proj_Id);
            ResultSet rs = stmt.executeQuery(query_4);
         
            
            // Fetch each row from the result set
             int count= 2;
            
            // Fetch each row from the result set
           while (rs.next()) {
                int i=0;
//5

// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        }
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }
    }*/
public void query4() throws IOException
    {

        try 
        {
             InputStreamReader i1 = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i1);
            stmt = conn.createStatement();
            String projects="";
            System.out.println("enter nunmber of projects");
            projects=stdin.readLine();
            ResultSet rs = stmt.executeQuery("select m.m_name,r.prj_id,m.m_id from F15_7_machinery m,F15_7_rent r where m.m_id=r.m_id and r.m_id in ( select r.m_id from F15_7_rent r group by m_id having count(r.prj_id)>="+projects+")");
            int count= 3;
            System.out.println("M_Name\tProject ID\tMachinery ID");
            // Fetch each row from the result set
           while (rs.next()) {
                int i=0;

// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }     
    }
 public void query5() throws IOException
    {

        try 
             InputStreamReader i1 = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i1);
            stmt = conn.createStatement();
            String dimension="";
            System.out.println("enter dimensions values of project");
            dimension=stdin.readLine();
            ResultSet rs = stmt.executeQuery("select p.name from F15_7_personnel p,F15_7_works_on_duration w, F15_7_project p1 where p.P_id=w.per_id and w.projectid=p1.proj_id and p1.proj_id in ( select proj_id from F15_7_project where (end_date-start_date) = ( select min(end_date-start_date) from F15_7_project where dimensions>"+dimension+"))");
            int count= 1;
            System.out.println("P_Name");
            // Fetch each row from the result set
           while (rs.next()) {
                int i=0;

// Display the Results
//
                 while(i<count){
	            i=i+1;
                    System.out.print(rs.getString(i)+"    \t");
                 }
                 System.out.println("\n");
              }
            stmt.close();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.toString());
        }     
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException
    {
        //Test.DBConnection();
        phase_5 j = new phase_5();
        j.DBConnection();
        
        String Choice = "";
        int scase = 0;
            
        InputStreamReader i = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(i);
        do
        {
            System.out.println("\n **********WELCOME TO BALAJI CONSTRUCTION COMPANY**********");
            System.out.println("\n\n Menu ");
            System.out.println("\n\n 1. Display tables.");
            System.out.println("\n 2. Insert into tables.");
            System.out.println("\n 3. Upadate tables.");
            System.out.println("\n 4. Delete.");
            System.out.println("\n 5. Generate Reports");
            System.out.println("\n 6. Quit");
            System.out.println("\n\n PLEASE ENTER YOUR CHOICE :");
            Choice=stdin.readLine();
            scase=Integer.parseInt(Choice);
            try{
                switch(scase)
                {
                    case 1:
                    {
                       // String data1;
                        //int x=-1;
                       // BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
                       // System.out.println("\n\n select the table name : ");
                        //data1=input.readLine();
                         int scase2=0;
                            do
                            {
                                
                                System.out.println("\n\n ********************************");
                               // System.out.println("\n HI "+data2+"! ");
                                System.out.println("\n\n *********TABLES*********");
                                System.out.println("\n\n 1. Project");
                                System.out.println("\n 2. Personnel");                                
                                System.out.println("\n 3. Supplier");
                                System.out.println("\n 4. Inventory");
                               
                                System.out.println("\n 5. Exit System.");
                                System.out.println("\n\n PLEASE ENTER YOUR CHOICE :");
                                scase2=Integer.parseInt(stdin.readLine());
                                
                    switch(scase2)
                                {
                                    case 1:
                                    {
                                       
                                        j.display_Project();
                                              
                                        break;
                                    }
                                    case 2:
                                    {
                                        j.display_Personnel();
                                        break;
                                    }
                                    case 3:
                                    {
                                      j.display_Supplier();
                                        break;
                                    }
                                    case 4:
                                    {
                                        
                                           j.display_Inventory();
                                        break;
                                    } 
                                }
                            }
                            while(scase2!=5);
                            break;
                }
                    case 2:
                    {
                        int scase3=0;
                        do{
                        System.out.println("Enter Which table to enter new data");
                        System.out.println("1.Personnel");
                        System.out.println("2.Project");
                        System.out.println("3.Inventory");
                        System.out.println("4.Back to main menu");
                        System.out.println("\n\n PLEASE ENTER YOUR CHOICE :");
                        scase3=Integer.parseInt(stdin.readLine());
                        switch(scase3)
                        
                                {
                            case 1:
                                    {
                                    String PID;
                      //int x;
                        System.out.println("Enter P_ID");
                        PID=stdin.readLine();
                        //int x;
                       // x=j.does_pid_exists(PID);
                      //  if(x==1){
                        //    System.out.println("P_ID already exists!!");
                        //}
                        //else{
                        j.new_Personnel(PID);
                        //}
                        break;
                                    }
                            case 2:
                                    {
                                        j.new_Project();
                                    break;
                                    }
                            case 3:
                                    j.new_Inventory();
                                    break;
                                   default:
                                   {
                                    System.out.println("Wrong Input Choice");
                                   break;
                                   }
                                }
                        }while(scase3!=4);
                        break;
                    }

                    case 3:
                    {
                        int scase4=0;
                        do{
                        System.out.println("Choose which cost to update");
                        System.out.println("1.Inventory");
                        System.out.println("2.Machinery");
                        System.out.println("3.Back to main menu");
                        scase4=Integer.parseInt(stdin.readLine());
                        switch(scase4)
                        {
                            case 1:
                                int x;
                                ///System.out
                               // x=j.does_I_id_exists()
                            j.update_Inventory_Cost();
                            break;
                            case 2:
                            j.update_Machinery_Cost();
                            break;
                            default:
                            System.out.println("Wrong Choice");
                            break;
                        }
                        }while(scase4!=3);
                            
                        break;
                    }
                    case 4:
                    {
                        int s4=0;
                        do{
                            //System.out.println("Choose which cost to update");
                        System.out.println("1.Delete personnel");
                        System.out.println("2.Back to main menu");
                        s4=Integer.parseInt(stdin.readLine());
                        switch(s4)
                        {
                            case 1:j.delete_Personnel();
                        break;
                            
                        }
                        }while(s4!=2);
                        break;
                        }
                        
                       
                    
                    case 5:
                    {
                    int scase5=0;
                    do{
                    System.out.println("Generate the following Reports:");
                    System.out.println("1. List all machinery used on a particular project and whose cost is greater than a particular value in descending order");
                    System.out.println("2. Retrieve Labaour Id and Type who works for maximum amount of time per day");
                    System.out.println("3. Give names of employees and positions working on a certain number of projects at the same time and have a salary greater than a particular value:");
                    System.out.println("4. List all machineries being used in more than X projects.");
                    System.out.println("5. Retrive all employee names who work on projects having least completion time and land area greater than X acres.");
                    System.out.println("6. Back to main menu");
                    System.out.println("\n\n PLEASE ENTER YOUR CHOICE :");
                    scase5=Integer.parseInt(stdin.readLine());
                    switch(scase5)
                    {
                                        case 1:
                                            j.query1();
                                            break;
                                        case 2:
                                            j.query2();
                                            break;
                                        case 3:
                                            j.query3();
                                            break;
                                        case 4:
                                            j.query4();
                                            break;
                                            
                                        case 5:
                                            j.query5();
                                            break;
                                        default:
                                            System.out.println("Wrong Input");
                                            break;
                                    }
                    }while(scase5!=6);
                    
                                    break;
                    }
                                  default:
                    {    
                        System.out.println("\nWRONG SELECTION");
                        break;
                    }
                    
            }
            }
            
            catch(Exception e){
            System.out.println(e);
            }
            
            }
        while(scase!=6);
    }

   
}
