# README #

Construction Management System

Problem Statement (REVISED)

Balaji Realty is a construction company, that aims at building apartments with great amenities. The company is planning to build a database system that helps them organize, plan and record any activity that is to be or is performed previously. It keeps track of information like inventory, laborers, and machinery. It also plans to store department specific information about laborers and financial payments. In addition it also stores fix land rate, cost of construction, supplier information and various variables.


Data Requirements (may not be complete)

1) Inventory details like sand, cement , steel, bricks and others. They will have attributes like item id, name, item type, cost of item, available stock quantity and the supplier information of each item. Inventory is supplied by the manager. Each inventory is associated with a particular project. Inventory can be supplied to the project more than one time.

2) Project details like name(id),location(zip code, area), construction area(land dimensions), and land cost of that particular area. In addition to this it will also have start date and completion date of the current project. The project can contain many apartments. We have various personnel working on the apartment, they can be either employees or laborers. The amount of time they work is also recorded corresponding to a particular date.

3)Personnel Information captures all the information about the people working on projects. We have two types of personnel, disjoint from one another a) Employee, b) Laborers. Personnel information records name, personnel id, address and their SSN number.
a) Employee information gives us details about employee id, project id they are working on currently, monthly salary and position of the employee. Ex. He’s a manager or an accountant etc.

b) Labor details include labor id, project they are working on, weekly salary and type of labor provided (plumbing, electricity, carpentry, painters etc).

4) Machinery includes type of machinery and rent of the machine per day. Machinery is used on projects. This machinery is provided by a supplier and the various dates associated are also given, ex. Start date and end date. A machine can be used several times on a particular project.

5) Supplier details contain supplier name, supplier id and the type of material supplied. The supplier may supply inventory to a project and also provide machinery to the project.

6) The apartment details include project name, apartment number, carpet area and Selling cost of the apartment and date of sale. A single apartment cannot be a part of more than one project. 


Processing Requirements (may not be complete)

All data captured above is used by the company to analyze and generate several reports that help them in proper functioning, reducing costs, increasing profits and managing their resources in the best possible manner. Some of the reports generated include

1) List of all available inventory and cost associated with buying the inventory. Shortage of inventory can also be calculated. This report will help us to save time to order the inventory in advance.

2) We can calculate profit/loss by verifying actual cost of building and estimated cost of building compared to the selling price of apartments in a project. Basic formula used is (land cost + employee salary+labor charges+ supplier payements >= Selling price of all the apartments)

3) To find out which project has more demand by calculating total number of apartments sold in each project.

4) Salary details about each employee helps us in foreseeing the payments due. We can also track where a particular employee is working, if free assign him a new project. 

5) Maintaining record of where machinery is currently being used. Scheduling of machinery can also be done. 

6) Total due amount for each supplier. Suppliers whose payments are due in the next 3 weeks are also found.

7) List all sold and vacant apartments to give a clear idea of profit estimations.

8) Estimation of completion time for a new project based on construction area and time of completion of previous projects.

Interaction Requirements 
We thought of this project done like a brick and mortar presence and not an internet application as it pertains to one single company and its need for solutions to certain problems in construction.