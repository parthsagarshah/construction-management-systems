---1. Find Total amount of inventory cost which is spent on it after 2 years of start date-----

select sum(i.cost*s.quantity) as sum, proj_id 
from F15_7_inventory i, F15_7_supply s, F15_7_project p 
where i.inv_id=s.in_id and p.proj_id=s.prj_id and s.s_date between p.start_date and add_months(p.start_date,24)
group by p.proj_id;



---2. Retrive all employee names who work on projects having least completion time and land area greater than 5 acres.---------
select p.name 
from F15_7_personnel p,F15_7_works_on_duration w, F15_7_project p1 
where p.P_id=w.per_id and w.projectid=p1.proj_id and p1.dimensions>5 and p1.proj_id in 
		( select proj_id 
		from F15_7_project 
		where (end_date-start_date) = 
			( select min(end_date-start_date) 
			from F15_7_project));

----3.List All the machinery being used in more than two projects-----------

select m.m_name,r.prj_id,m.m_id 
from F15_7_machinery m,F15_7_rent r 
where m.m_id=r.m_id and r.m_id in 
		( select r.m_id 
		from F15_7_rent r 
		group by m_id 
		having count(r.prj_id)>=2);


------4 Retrieve all apartment numbers and their respective project id's which have been sold during the period of construction--------
select project_id, count(apt_no)
from F15_7_apartment a, F15_7_project p 
where a.sale_date between p.start_date and p.end_date and a.project_id=p.proj_id
group by a.project_id;

----5.Retrive all project id and names where maximum number of personnel are working--------------------
select proj_id,pname 
from F15_7_project 
where proj_id in 
		(select projectid 
		from F15_7_works_On_Duration 
		group by projectid 
		having count(per_id)=
				(select max(count(per_id)) 
				from F15_7_works_on_duration 
				group by projectid)); 




------6. Retrieve the labourer id and type who works for maximum amount of time per day----------

select l_id,type 
from F15_7_labourer l, F15_7_works_On_Duration w  
where l_id=w.per_id and w.per_id in 
		(select per_id
		 from F15_7_works_On_Duration w,F15_7_labourer l 
		where w.per_id=l.l_id and w.hoursperday = 
				(select max(hoursperday) 
				from F15_7_works_On_Duration));


-----7. Retirive the employe id and their position that work in multiple project--------------
select w.per_id, 
	(select E.position 
	from F15_7_employee E 
	where E.E_ID = w.per_id) AS "Employee position", COUNT (*) AS "No of Projects"
from F15_7_works_On_Duration w, F15_7_employee e 
where e.e_id=w.per_id 
group by (w.per_id)
having count (*) > 1;



