-----------script to update some data in the tables---------
insert into F15_7_personnel values (6611, 'Rishi Karia');

insert into F15_7_employee values (6611, 60000, 'Project lead');
insert into F15_7_personnel values (6612, 'abbas plumber');
insert into F15_7_labourer values (6612, 1200, 'Plumber');
insert into F15_7_works_On_Duration(6611,001, DATE '2011-08-01', DATE '2012-02-01');
insert into F15_7_works_On_Duration(6611,003, DATE '2011-08-01', DATE '2012-02-01');

insert into F15_7_machinery values  ( 218,'dumper',2200);

insert into F15_7_rent  values   (222,218,007,9,DATE '2013-07-07',DATE '2014-02-13');
insert into F15_7_rent  values   (229,218,004,9,DATE '2013-09-16',DATE '2014-01-01');

------------1.delete plumbbers whose weekly salary is 950------------
delete from F15_7_personnel
where p_id in
	(select l_id
	from F15_7_labourer
	where type='plumber' and weekly_sal=950);
 


----------2. Due to high demands of drill cost is increased by 50%----------
update F15_7_machinery
set cost_perday=cost_perday*1.5
where m_id in
	(select m_id
	from F15_7_machinery
	where m_name='drill');


