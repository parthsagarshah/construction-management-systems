----------Script to insert the sample data to all tables----------
-------F15_7_project------
insert into F15_7_project values (001,'Valencia',35000000,'viman nagar',15000000,6.5,DATE '2011-05-01',DATE '2012-08-01');
insert into F15_7_project values (002,'Greenland',70000000,'kalyani nagar',25000000,9.2,DATE '2011-01-03',DATE '2012-04-04');
insert into F15_7_project values (003,'Daffodils',42000000,'aundh',18000000,7,DATE '2013-03-05',DATE '2015-09-01');
insert into F15_7_project values (004,'Serenity',90000000,'bavdhan',35000000,12,DATE '2014-11-03',DATE '2015-12-22');
insert into F15_7_project values (005,'Tranquility',20000000,'banjara hills',5000000,3,DATE '2013-06-29',DATE '2014-02-15');
insert into F15_7_project values (006,'Queensland',78000000,'jubilee hills',38000000,10,DATE '2014-02-09',DATE '2015-11-2');
insert into F15_7_project values (007,'Meadows',53000000,'bits',22000000,4.5,DATE '2011-12-19',DATE '2013-11-01');
insert into F15_7_project values (008,'Cooper',20000000,'gachibowli',10000000,3.2,DATE '2012-01-17',DATE '2012-12-13');
insert into F15_7_project values (009,'Nebula',42000000,'hitech city',25000000,5.5,DATE '2013-04-07',DATE '2014-06-19');
insert into F15_7_project values (010,'Antriksh',50000000,'kharadi',32000000,3.9,DATE '2012-08-22',DATE '2014-11-28');


-----F15_7_supplier----
insert into F15_7_supplier values (222,'Dksuppliers');
insert into F15_7_supplier values (223,'Rdx Co');
insert into F15_7_supplier values (224,'Longhorn assocites');
insert into F15_7_supplier values  (225,'Deluxfittings');
insert into F15_7_supplier values  (226,'Inside out');
insert into F15_7_supplier values  (227,'Laxmi Suppliers');
insert into F15_7_supplier values  (228,'Kohler');
insert into F15_7_supplier values  (229,'Weikfield');
insert into F15_7_supplier values  (230,'SunShine');
insert into F15_7_supplier values  (231,'Geico');
insert into F15_7_supplier values  (232,'Woodchoppers');
insert into F15_7_supplier values  (233,'RealSteal');


-----------F15_7_personnel----
insert into F15_7_personnel values  (6682,'Parth Shah');
insert into F15_7_personnel values  (6683,'Rohit Chawla');
insert into F15_7_personnel values  (6684,'Rahul George');
insert into F15_7_personnel values  (6685,'Kartik Shirke');
insert into F15_7_personnel values  (6686,'Sreedhar Reddy');
insert into F15_7_personnel values  (6687,'Ankit Shetty');
insert into F15_7_personnel values  (6688,'Bhavik Shah');
insert into F15_7_personnel values  (6689,'Ankit Mohla');
insert into F15_7_personnel values  (6690,'Shivesh Singh');
insert into F15_7_personnel values  (6691,'Angad Bashani');
insert into F15_7_personnel values  (6692,'Shubham Iyengar');
insert into F15_7_personnel values  (6693,'Imran Rehali');
insert into F15_7_personnel values  (6694,'Rahul Prakash');
insert into F15_7_personnel values  (6695,'Somesh Ganesh');
insert into F15_7_personnel values  (6696,'Akshay Lulla');
insert into F15_7_personnel values  (6697,'Areeb Iqbal');
insert into F15_7_personnel values  (6698,'Akshay Chordia');
insert into F15_7_personnel values  (6699,'Vijay Lala');
insert into F15_7_personnel values  (6700,'Luv Sharma');


-------F15_7_employee-------
insert into F15_7_employee values (6682,20000,'Site Manager');
insert into F15_7_employee values (6683,24000,'Site Manager');
insert into F15_7_employee values (6684,15000,'Senior Emp');
insert into F15_7_employee values (6685,17000,'IT Worker');
insert into F15_7_employee values (6686,16500,'Advertising Head');
insert into F15_7_employee values (6687,22000,'Architect');
insert into F15_7_employee values (6688,21000,'Civil Engineer');
insert into F15_7_employee values (6689,19000,'Civil Engineer');
insert into F15_7_employee values (6690,23000,'Supervisior');



-------F15_7_labourer values-------
insert into F15_7_labourer values  (6691,1500,'carpenter');
insert into F15_7_labourer values  (6692,1350,'carpenter');
insert into F15_7_labourer values  (6693,950,'plumber');
insert into F15_7_labourer values  (6694,1250,'plumber');
insert into F15_7_labourer values  (6695,1800,'tile fittings');
insert into F15_7_labourer values  (6696,1900,'tile fittings');
insert into F15_7_labourer values  (6697,2250,'cement worker');
insert into F15_7_labourer values  (6698,2100,'cement worker');
insert into F15_7_labourer values  (6699,2010,'electrician');
insert into F15_7_labourer values  (6700,1900,'electrician');


-----F15_7_machinery values --------
insert into F15_7_machinery values  ( 201,'drill',1500);
insert into F15_7_machinery values  ( 202,'drill',1500);
insert into F15_7_machinery values  ( 203,'drill',1500);
insert into F15_7_machinery values  ( 204,'cement mixer',1895);
insert into F15_7_machinery values  ( 205,'cement mixer',1895);
insert into F15_7_machinery values  ( 206,'cement mixer',1895);
insert into F15_7_machinery values  ( 207,'cement mixer',1895);
insert into F15_7_machinery values  ( 208,'Jcb',20000);
insert into F15_7_machinery values  ( 209,'Jcb',20000);
insert into F15_7_machinery values  ( 210,'Jcb with drill',29000);
insert into F15_7_machinery values  ( 211,'Jcb with drill',29000);
insert into F15_7_machinery values  ( 212,'Fork lift',3500);
insert into F15_7_machinery values  ( 213,'Fork lift',3500);
insert into F15_7_machinery values  ( 214,'Fork lift',3500);
insert into F15_7_machinery values  ( 215,'dumper',2500);
insert into F15_7_machinery values  ( 216,'dumper',2500);
insert into F15_7_machinery values  ( 217,'dumper',2500);

------F15_7_inventory values -----



insert into F15_7_inventory values  ( 531, 'Tile', 'Vetrified', 600);
insert into F15_7_inventory values  ( 532, 'Tile', 'Marble', 1200);
insert into F15_7_inventory values  ( 533, 'Tile', 'Granite', 1500);
insert into F15_7_inventory values  ( 534, 'Tile', 'Spartex', 400);
insert into F15_7_inventory values  ( 535, 'Sand', 'Corse', 450);
insert into F15_7_inventory values  ( 536, 'Sand', 'Fine', 300);
insert into F15_7_inventory values  ( 537, 'Sand', 'Granular', 500);
insert into F15_7_inventory values  ( 538, 'Sand', 'Stone', 200);
insert into F15_7_inventory values  ( 539, 'Sand', 'Red', 900);
insert into F15_7_inventory values  ( 540, 'Doors', 'Vetrified', 600);
insert into F15_7_inventory values  ( 541, 'Doors', 'Vetrified', 600);
insert into F15_7_inventory values  ( 542, 'Doors', 'Vetrified', 600);
insert into F15_7_inventory values  ( 543, 'Window fittings', 'Square', 1300);
insert into F15_7_inventory values  ( 544, 'Window fittings', 'Rectangular', 1800);
insert into F15_7_inventory values  ( 545, 'Cement', 'Quick Dry', 900);
insert into F15_7_inventory values  ( 546, 'Cement', 'Low Heat hydration', 1000);
insert into F15_7_inventory values  ( 547, 'Cement', 'Normal', 800);
insert into F15_7_inventory values  ( 548, 'Cement', 'High early strength', 1100);
insert into F15_7_inventory values  ( 549, 'Gravel', 'Marble Chips', 1700);
insert into F15_7_inventory values  ( 550, 'Gravel', 'Crushed Stone', 500);
insert into F15_7_inventory values  ( 551, 'Gravel', 'Pea Gravel', 1700);
insert into F15_7_inventory values  ( 552, 'Gravel', 'Balast', 2000);


-----F15_7_works_On_Duration  values -------

insert into F15_7_works_On_Duration  values   (6691,001,DATE '2011-08-01',DATE '2012-08-01',4);
insert into F15_7_works_On_Duration  values   (6692,002,DATE '2011-03-23',DATE '2012-04-04',7);
insert into F15_7_works_On_Duration  values   (6693,003,DATE '2014-09-11',DATE '2015-12-22',10);
insert into F15_7_works_On_Duration  values   (6682,004,DATE '2015-08-22',DATE '2012-08-01',9);
insert into F15_7_works_On_Duration  values   (6683,005,DATE '2013-08-31',DATE '2013-12-01',9);
insert into F15_7_works_On_Duration  values   (6684,007,DATE '2012-01-01',DATE '2012-05-21',9);
insert into F15_7_works_On_Duration  values   (6685,010,DATE '2012-11-21',DATE '2013-07-15',7);
insert into F15_7_works_On_Duration  values   (6682,001,DATE '2012-02-23',DATE '2012-08-01',4);
insert into F15_7_works_On_Duration  values   (6691,002,DATE '2012-02-28',DATE '2012-05-22',5);
insert into F15_7_works_On_Duration  values   (6692,006,DATE '2014-05-28',DATE '2015-03-16',5);

------F15_7_suppply  values ------
insert into F15_7_suppply  values   (222,531,001,DATE '2011-07-21',150);
insert into F15_7_suppply  values   (225,552,002,DATE '2011-09-13',300);
insert into F15_7_suppply  values   (230,541,005,DATE '2013-11-11',250);
insert into F15_7_suppply  values   (231,550,006,DATE '2014-08-31',175);
insert into F15_7_suppply  values   (227,547,002,DATE '2011-10-17',400);
insert into F15_7_suppply  values   (225,535,007,DATE '2012-01-12',190);
insert into F15_7_suppply  values   (223,537,008,DATE '2012-03-15',375);
insert into F15_7_suppply  values   (229,548,010,DATE '2013-05-18',125);
insert into F15_7_suppply  values   (233,541,006,DATE '2015-02-25',350);
insert into F15_7_suppply  values   (229,550,002,DATE '2011-01-30',275);
insert into F15_7_suppply  values   (224,552,008,DATE '2014-03-28',215);


-------F15_7_rent  values-------
insert into F15_7_rent  values   (230,201,001,3,DATE '2011-07-23',DATE '2011-10-25');
insert into F15_7_rent  values   (222,211,004,2,DATE '2013-09-20',DATE '2014-01-12');
insert into F15_7_rent  values   (227,210,002,4,DATE '2012-02-14',DATE '2012-04-04');
insert into F15_7_rent  values   (229,209,007,1,DATE '2012-11-02',DATE '2013-02-05');
insert into F15_7_rent  values   (231,204,008,3,DATE '2012-07-16',DATE '2012-10-15');
insert into F15_7_rent  values   (227,203,009,5,DATE '2013-09-18',DATE '2013-12-19');
insert into F15_7_rent  values   (232,202,001,6,DATE '2014-10-09',DATE '2015-02-23');
insert into F15_7_rent  values   (229,212,002,2,DATE '2011-07-20',DATE '2012-03-21');
insert into F15_7_rent  values   (224,217,003,1,DATE '2014-07-08',DATE '2015-04-19');
insert into F15_7_rent  values   (229,206,005,3,DATE '2013-06-29',DATE '2013-08-16');
insert into F15_7_rent  values   (222,203,006,5,DATE '2015-01-06',DATE '2015-11-11');
insert into F15_7_rent  values   (232,201,007,9,DATE '2013-07-07',DATE '2014-02-13');


--------F15_7_apartment values------
insert into F15_7_apartment values (1001, 1600,4600000,DATE '2011-12-04',001);
insert into F15_7_apartment values (1002, 1800,6500000,DATE '2012-03-17',001);
insert into F15_7_apartment values (1003, 1100,3600000,DATE '2012-02-28',001);
insert into F15_7_apartment values (1004, 600, 2500000,DATE '2011-11-28',002);
insert into F15_7_apartment values (1005, 1300,4000000,DATE '2011-03-27',002);
insert into F15_7_apartment values (1006, 1650,6000000,DATE '2014-08-22',003);
insert into F15_7_apartment values (1007, 1790,6700000,DATE '2015-01-02',003);
insert into F15_7_apartment values (1008, 2200,9000000,DATE '2013-12-16',003);
insert into F15_7_apartment values (1009, 1580,5600000,DATE '2014-12-19',004);
insert into F15_7_apartment values (1010, 1440,5200000,DATE '2015-02-23',005);
insert into F15_7_apartment values (1011, 1320,3800000,DATE '2015-09-18',005);
insert into F15_7_apartment values (1012, 1250,4200000,DATE '2015-06-12',006);
insert into F15_7_apartment values (1013, 1550,4900000,DATE '2014-08-31',006);
insert into F15_7_apartment values (1014, 1890,7950000,DATE '2015-05-05',006);
insert into F15_7_apartment values (1015, 1675,4580000,DATE '2012-01-25',007);
insert into F15_7_apartment values (1016, 1730,6900000,DATE '2011-10-12',007);
insert into F15_7_apartment values (1017, 1565,4300000,DATE '2013-04-05',007);
insert into F15_7_apartment values (1018, 1995,8000000,DATE '2012-04-20',008);
insert into F15_7_apartment values (1019, 2300,10000000,DATE '2013-11-14',009);
insert into F15_7_apartment values (1020, 1325,4470000,DATE '2014-05-09',009);
insert into F15_7_apartment values (1021, 2250,9080000,DATE '2012-12-31',010);
