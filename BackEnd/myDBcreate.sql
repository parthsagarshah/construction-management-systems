------creating relations---------


create table F15_7_project
(
Proj_ID integer NOT NULL,
PName varchar(20) NOT NULL,
Estd_Budget integer,
Location varchar(20),
Land_Cost integer,
Dimensions float,
Start_Date date,
End_Date date,
Primary key(Proj_ID)
);

create table F15_7_personnel
(
P_ID integer NOT NULL,
Name varchar(20) NOT NULL,
Primary key(P_ID)
);

create table F15_7_supplier
(
Supplier_ID integer NOT NULL,
S_Name varchar(20) NOT NULL,
Primary Key(Supplier_ID)
);

create table F15_7_inventory
(
Inv_Id integer NOT NULL,
I_Name varchar(20) NOT NULL,
Type varchar(20) NOT NULL,
Cost varchar(20) NOT NULL,
Primary Key(Inv_Id)
);

create table F15_7_machinery
(
M_ID integer NOT NULL,
M_Name varchar(20) NOT NULL,
Cost_perDay integer NOT NULL,
Primary key(M_ID)
);

create table F15_7_apartment
(
Apt_no integer NOT NULL,
Apt_Area integer NOT NULL,
Selling_Cost integer,
Sale_date date,
Project_ID integer NOT NULL,
Primary Key(Apt_no),
Foreign key (Project_ID) references project(Proj_ID)
On delete cascade
);

create table F15_7_employee
(
E_ID integer NOT NULL,
Montly_Sal integer NOT NULL,
Position varchar(20),
Primary Key(E_ID),
Foreign key (E_ID) references personnel(P_ID)
On delete cascade
);


create table F15_7_labourer
(
L_ID integer NOT NULL,
Weekly_Sal integer NOT NULL,
Type varchar(15),
Primary Key(L_ID),
Foreign key (L_ID) references personnel(P_ID)
On delete cascade
);

create table F15_7_works_On_Duration
(
Per_ID integer NOT NULL,
ProjectID integer NOT NULL,
Start_day date NOT NULL,
End_day date NOT NULL,
HoursperDay integer NOT NULL,
Primary key(Per_ID,ProjectID,Start_day,End_day,HoursperDay),
Foreign key (Per_ID) references personnel(P_ID)
On delete cascade,
Foreign key (ProjectID) references project(Proj_ID)
On delete cascade
);


create table F15_7_supply
(
Supp_ID integer NOT NULL,
In_ID integer NOT NULL,
Prj_ID integer NOT NULL,
S_Date date NOT NULL,
Quantity integer NOT NULL,
Primary Key(Supp_ID,In_ID,Prj_ID,S_Date,Quantity),
Foreign key (Supp_ID) references supplier(Supplier_ID)
On delete cascade,
Foreign key (In_ID) references inventory(Inv_ID)
On delete cascade,
Foreign key (Prj_ID) references project(Proj_ID)
On delete cascade
);

create table F15_7_rent
(
Supp_ID integer NOT NULL,
M_ID integer NOT NULL,
Prj_ID integer NOT NULL,
Qty integer NOT NULL,
St_date date NOT NULL,
End_date date NOT NULL,
Primary Key(Supp_ID,M_ID,Prj_ID,Qty,St_date,End_date),
Foreign key (Supp_ID) references supplier(Supplier_ID)
On delete cascade,
Foreign key (M_ID) references Machinery(M_ID)
On delete cascade,
Foreign key (Prj_ID) references project(Proj_ID)
On delete cascade
);




